#ifndef USE_POINTERS
#define DYNMEM ALLOCATABLE
#else
#define DYNMEM POINTER
#endif

MODULE GRD_STR

!-----------------------------------------------------------------------
!                                                                      !
! STRUCTURE OF THE GRID                                                !
!                                                                      !
! VERSION 1: S.DOBRICIC 2006                                           !
!
! A.S. : NOW CONTROL VECTORS AND THEIR ADJOINTS IN PHYSICAL SPACE
! ARE JUST POINTERS TO A GLOBAL ARRAY, IN ORDER TO OPTIMIZE THE
! OPENMP PARALLELIZATION AND PERMIT INDEPENDENT TREATMENT OF T AND S
! IN THE RECURSIVE FILTER ROUTINES.
!-----------------------------------------------------------------------

 USE SET_KND

IMPLICIT NONE

PUBLIC

   TYPE GRID_T

        INTEGER(I4)              ::  IM           ! NO. POINTS IN X DIRECTION
        INTEGER(I4)              ::  JM           ! NO. POINTS IN Y DIRECTION
        INTEGER(I4)              ::  KM           ! NO. POINTS IN Z DIRECTION
        INTEGER(I4)              ::  NPS          ! NO. OF OCEAN POINTS

        REAL(R8),    DYNMEM     ::  MSK(:,:,:)    ! SEA-LAND MASK
        REAL(R8),    DYNMEM     ::  MSK2(:,:,:)    ! SEA-LAND MASK

        REAL(R8),    DYNMEM     ::  LON(:,:)       ! LONGITUDE
        REAL(R8),    DYNMEM     ::  LAT(:,:)       ! LATITUDE
        REAL(R8),    DYNMEM     ::  BAT(:,:)       ! LATITUDE
        REAL(R8),    DYNMEM     ::  DEP(:)       ! DEPTH
        REAL(R8),    DYNMEM     ::  DX(:,:)       ! DEPTH
        REAL(R8),    DYNMEM     ::  DY(:,:)       ! DEPTH

   END TYPE GRID_T

   TYPE (GRID_T)                 :: GRD
   TYPE (GRID_T)                 :: GRD_HR

END MODULE GRD_STR
