SUBROUTINE ANGLE_DIST(LX,LY,XLON_T,XLAT_T,XLON_U,XLAT_U,COS_T,SIN_T)
  !!
  !!
  !!========================================================================
  !!
  !!                     *******  ANGLE_DIST  *******
  !!
  !! GIVEN THE COORDINATES AT T-POINTS AND U-POINTS, THIS ROUTINE
  !COMPUTES 
  !! SINUS AND COSINUS OF THE ANGLE OF THE LOCAL GRID DISTORSION AT
  !T-POINTS
  !!
  !!
  !! INPUT :     - LX     = X DIMENSION                     [INTEGER]
  !! -------     - LY     = Y DIMENSION                     [INTEGER]
  !!             - XLON_T = LONGITUDE ARRAY AT T-POINTS
  ![ARRAY(LX,LY) OF REAL]
  !!             - XLAT_T = LATITUDE ARRAY AT T-POINTS
  ![ARRAY(LX,LY) OF REAL]
  !!             - XLON_U = LONGITUDE ARRAY AT U-POINTS
  ![ARRAY(LX,LY) OF REAL]
  !!             - XLAT_T = LATITUDE ARRAY AT U-POINTS
  ![ARRAY(LX,LY) OF REAL]
  !!
  !! OUTPUT :
  !! --------    - COS_T  = COSINUS OF THE DISTORTION ANGLE
  ![ARRAY(LX,LY) OF REAL]
  !!             - SIN_T  = SININUS OF THE DISTORTION ANGLE
  ![ARRAY(LX,LY) OF REAL]
  !!
  !!
  !! AUTHOR : LAURENT BRODEAU (BRODEAU@HMG.INPG.FR)
  !!          DIRECTLY INSPIRED FROM 'GEO2OCEAN.F90' (OPA/MADEC)
  !!
  !!========================================================================
  !!
  !!
  USE SET_KND
  IMPLICIT NONE
  !!
  !!
  !! INPUT :
  !! -------
  INTEGER, INTENT(IN) :: LX, LY       ! DIMENSION OF ARRAYS
  !!
  REAL(R8), DIMENSION(LX,LY), INTENT(IN) ::    &
       &       XLON_T, XLON_U,   &  ! LATITUDES  AT POINT T AND U
       &       XLAT_T, XLAT_U       ! LONGITUDES AT POINT T AND U 
  !!
  !!
  !! OUTPUT :
  !! --------
  REAL(R8), DIMENSION(LX,LY), INTENT(OUT) :: COS_T, SIN_T
  !!
  !!
  !! LOCAL :
  !! -------
  INTEGER :: JI
  !!
  REAL(R8), DIMENSION(LY) :: &
       &       ZLON, ZLAT, ZXNPT, ZNNPT, ZLAN, &
       &       ZPHH, ZXUUT, ZYUUT, ZMNPUT, ZYNPT
  !!
  REAL(R8), PARAMETER :: &
       &       PI  = 3.14159265359_R8,&
       &       RAD = PI/180._R8
  !!
  !!
  !!
  DO JI = 1, LX
     !!
     !! NORTH POLE DIRECTION & MODULOUS (AT T-POINT) :
     !! ----------------------------------------------
     ZLON  = XLON_T(JI,:)
     ZLAT  = XLAT_T(JI,:)
     ZXNPT = 0._R8 - 2._R8*COS( RAD*ZLON )*TAN(PI/4._R8 - RAD*ZLAT/2._R8)
     ZYNPT = 0._R8 - 2._R8*SIN( RAD*ZLON )*TAN(PI/4._R8 - RAD*ZLAT/2._R8)
     ZNNPT = ZXNPT*ZXNPT + ZYNPT*ZYNPT
     !!
     !!
     !!
        !! "I" DIRECTION & MODULOUS (AT T-POINT) :
     !! ---------------------------------------
     !!   ( SINCE WE DEAL WITH T POINTS WE LOOK AT U POINT 
     !!     ON A V POINT WE WOULD LOOK AT THE F POINT )
     !!
     ZLON = XLON_U(JI,:)
     ZLAT = XLAT_U(JI,:)
     !!
     IF ( JI == 1 ) THEN
        ZLAN = XLON_U(LX-3,:)          !! PERIODICITY OF ORCA GRID
        ZPHH = XLAT_U(LX-3,:)          !! WITH OVERLAP OF 2 POINTS
     ELSE
        ZLAN = XLON_U(JI-1,:)
        ZPHH = XLAT_U(JI-1,:)
     END IF
     !!
     !!
     ZXUUT  = 2._R8*COS(RAD*ZLON) &
          & *TAN(PI/4._R8-RAD*ZLAT/2._R8)-2._R8*COS(RAD*ZLAN)*TAN(PI/4._R8-RAD*ZPHH/2._R8)
     ZYUUT  = 2._R8*SIN(RAD*ZLON) &
          & *TAN(PI/4._R8-RAD*ZLAT/2._R8)-2._R8*SIN(RAD*ZLAN)*TAN(PI/4._R8-RAD*ZPHH/2._R8)
     ZMNPUT = SQRT(ZNNPT*(ZXUUT*ZXUUT + ZYUUT*ZYUUT))
     !!
     WHERE ( ZMNPUT < 1.E-14_R8 ) ZMNPUT = 1.E-14_R8
     !!
     !!
     !! COSINUS AND SINUS USING SCALAR AND VECTORIAL PRODUCTS :
     !! -------------------------------------------------------
     !!   (CAUTION, ROTATION OF 90 DEGRES)
     SIN_T(JI,:) =  ( ZXNPT*ZXUUT + ZYNPT*ZYUUT ) / ZMNPUT
     COS_T(JI,:) = -( ZXNPT*ZYUUT - ZYNPT*ZXUUT ) / ZMNPUT
     !!
  END DO
  !!
  !!
END SUBROUTINE ANGLE_DIST

