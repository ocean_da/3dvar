source("~/R_library/aux.R")
v<-"topo"
vl<-"lsm"
topo<-get_fld("topo_REG16.nc",v)
lsm<-get_fld("topo_REG16.nc",vl)
nx<-dim(lsm)[1]
ny<-dim(lsm)[2]
dep<-get_fld("GRID_HR.nc","dep")
nz<-length(dep)

tmsk<-array(0,dim=c(nx,ny,nz))
topo[topo <= 1]<-0
for (k in 1:(nz-1)){
  print(k)
  h<-tmsk[,,k]
  h[topo>=dep[k]]<-1
  tmsk[,,k]<-h
}
put_fld("tmsk_REG16.nc","tmsk",tmsk)
