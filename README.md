# 3dvar

3DVAR Main repository, containing:

- Main assimilation code (./src) and include files (./include)
- Tools for EOFs, interpolation, and domain rebuilding
