SUBROUTINE POSTPROC_EOFS(N_X,N_Y,N_R,N_XM,N_YM,N_Z,N_EOF,N_KMT,ZEIVAL,ZEIVEC,N2,N3,CPR)

USE SET_KND
USE MYNETCDF

IMPLICIT NONE

INTEGER(KIND=I4), INTENT(IN) :: N_X,N_Y,N_R,N_XM,N_YM,N_Z,N_EOF,N_KMT
REAL(KIND=R8), INTENT(IN) :: ZEIVAL(N_R,N_EOF),ZEIVEC(N_R,N_KMT,N_EOF)
INTEGER(KIND=I4), INTENT(IN) :: N2, N3
CHARACTER(LEN=*) :: CPR
!
INTEGER(KIND=I4) :: IREG(N_XM,N_YM)
INTEGER(KIND=I4) :: N_OFFS, NIND, JREG
INTEGER(KIND=I4) :: NBYX,NBYY
REAL(KIND=R8), ALLOCATABLE :: ZTEMP(:,:),ZMSK(:,:,:),ZVCORR(:,:),&
& ZVCORR3(:,:,:),ZTEMP3(:,:,:),ZTE(:,:,:)
INTEGER(KIND=I4), ALLOCATABLE :: COU(:,:),CCORR(:)
INTEGER(KIND=I4) :: JJ, JI, JL, JP, JLI, JL2, JLI2, JMP, JS, JE
INTEGER(KIND=I4), PARAMETER :: MAXS=9,MAXC=24
INTEGER(KIND=I4) :: NFDS,NMINS(MAXS),NMAXS(MAXS),NNMP,NLI1(MAXC)
CHARACTER(LEN=30) :: CNAMS(MAXS),CMPNAM(MAXC)
REAL(KIND=R8) :: ZSUM,ZCORR,ZCOVAR
LOGICAL :: LLSQUARED
LOGICAL :: LL_MAP_STDEV, LL_SEC_STDEV, LL_SEC_CORRS, LL_MAP_CORRS

REAL(R8) :: TMSK(N_XM,N_YM,N_Z)
REAL(R8) :: LON(N_XM,N_YM)
REAL(R8) :: LAT(N_XM,N_YM)
REAL(R8) :: DX(N_XM,N_YM)
REAL(R8) :: DY(N_XM,N_YM)
REAL(R8) :: DZ(N_Z)

NAMELIST / DIA / NFDS,NMINS,NMAXS,CNAMS,NNMP,NLI1,CMPNAM,NBYX,NBYY
NAMELIST / OPT / LL_MAP_STDEV, LL_SEC_STDEV, LL_SEC_CORRS, LL_MAP_CORRS

CALL GETNCVAR('GRID.nc','tmsk',N_XM,N_YM,N_Z,TMSK)
CALL GETNCVAR('GRID.nc','lon',N_XM,N_YM,LON)
CALL GETNCVAR('GRID.nc','lat',N_XM,N_YM,LAT)
CALL GETNCVAR('GRID.nc','dx',N_XM,N_YM,DX)
CALL GETNCVAR('GRID.nc','dy',N_XM,N_YM,DY)
CALL GETNCVAR('GRID.nc','dz',N_Z,DZ)

WRITE(*,*) ' READING NML'

LL_MAP_STDEV = .TRUE.
LL_SEC_STDEV = .TRUE.
LL_SEC_CORRS = .TRUE.
LL_MAP_CORRS = .TRUE.

OPEN(16,FILE='namelist')
REWIND(16)
READ(16,OPT)
REWIND(16)
READ(16,DIA)
CLOSE(16)

WRITE(*,DIA)

ALLOCATE(ZMSK(N_XM,N_YM,N_KMT))
IF( N2.GT.0 ) THEN
  DO JL=1,N2
    ZMSK(:,:,JL)=TMSK(:,:,1)
  ENDDO
ENDIF
DO JL=1,N3
    JS=N2+(JL-1)*N_Z+1
    JE=N2+(JL)*N_Z
    ZMSK(:,:,JS:JE)=TMSK(:,:,1:N_Z)
ENDDO

IF( N2 .NE. 0 .AND. N2 .NE. 1 ) CALL ABOR1('N2>1 NOT SUPPORTED IN POSTPROC MODE')
IF( N3 .NE. 2 .AND. N3 .NE. 4 ) CALL ABOR1('N4 != 2 AND 4 NOT SUPPORTED IN POSTPROC MODE')

WRITE(*,*) ' DIAGNOSTICS'

ALLOCATE(ZTEMP(N_XM,N_YM))
ALLOCATE(ZTEMP3(N_XM,N_YM,N_KMT))
ZTEMP3=0._R8

JREG=0
DO JJ=1,N_YM,NBYY
   DO JI=1,N_XM,NBYX
     JREG=JREG+1
     IREG(JI:MIN(JI+NBYX-1,N_XM),JJ:MIN(JJ+NBYY-1,N_YM))=JREG
   ENDDO
ENDDO

WRITE(*,*) ' DIMENSIONS ', N_XM, N_YM,N_KMT
WRITE(*,*) ' MIN,MAX IRE', MINVAL(IREG),MAXVAL(IREG)

DO JL=1,N_KMT
   DO JJ=1,N_YM
      DO JI=1,N_XM
        ZTEMP3(JI,JJ,JL) = ZTEMP3(JI,JJ,JL) + &
        & SQRT(SUM( ZEIVEC(IREG(JI,JJ),JL,:) * ZEIVAL(IREG(JI,JJ),:) * &
        & ZEIVEC(IREG(JI,JJ),JL,:) ))
      ENDDO
   ENDDO
ENDDO

OPEN(72,FILE='all_outnc')

IF(LL_MAP_STDEV) THEN

 IF( N2 .EQ. 1 ) THEN
   CALL WRITE_VARNCDF(TRIM(CPR)//'ssh_STDEV2D.nc',N_XM,N_YM,ZTEMP3(:,:,1)*&
   & ZMSK(:,:,1),'hsd','m','Sea-surface height standard deviation')
   WRITE(72,*) 'ssh_STDEV2D.nc'
 ENDIF

 CALL WRITE_VARNCDF(TRIM(CPR)//'tem_STDEV3D.nc',N_XM,N_YM,N_Z,ZTEMP3(:,:,1+N2:N_Z+N2)*&
 & ZMSK(:,:,1+N2:N_Z+N2),'tsd','degC','Temperature standard deviation')
 CALL WRITE_VARNCDF(TRIM(CPR)//'sal_STDEV3D.nc',N_XM,N_YM,N_Z,ZTEMP3(:,:,N_Z+1+N2:2*N_Z+N2)*&
 & ZMSK(:,:,N_Z+1+N2:2*N_Z+N2),'ssd','PSU','Salinity standard deviation')
 WRITE(72,*) 'tem_STDEV3D.nc'
 WRITE(72,*) 'sal_STDEV3D.nc'

 IF( N3 .EQ. 4 ) THEN
   CALL WRITE_VARNCDF(TRIM(CPR)//'uzc_STDEV3D.nc',N_XM,N_YM,N_Z,ZTEMP3(:,:,2*N_Z+N2+1:3*N_Z+N2)*&
   & ZMSK(:,:,2*N_Z+N2+1:3*N_Z+N2),'usd','m s-1','U-current standard deviation')
   CALL WRITE_VARNCDF(TRIM(CPR)//'vmc_STDEV3D.nc',N_XM,N_YM,N_Z,ZTEMP3(:,:,3*N_Z+N2+1:4*N_Z+N2)*&
   & ZMSK(:,:,3*N_Z+N2+1:4*N_Z+N2),'vsd','m s-1','V-current standard deviation')
   WRITE(72,*) 'uzc_STDEV3D.nc'
   WRITE(72,*) 'vmc_STDEV3D.nc'
 ENDIF

DO JP=1,NFDS

 ZSUM=SUM(DZ(NMINS(JP):NMAXS(JP)))

 ZTEMP=0._R8
 N_OFFS=N2
 DO JL=NMINS(JP)+N_OFFS,NMAXS(JP)+N_OFFS
      ZTEMP = ZTEMP + &
      & ZTEMP3(:,:,JL)*DZ(JL-N_OFFS)/ZSUM
 ENDDO
 CALL WRITE_VARNCDF(TRIM(CPR)//'tem_'//TRIM(CNAMS(JP))//'.nc',N_XM,N_YM,ZTEMP,'tsd','degC',&
 & 'Temperature standard deviation')

 WRITE(72,*) 'tem_'//TRIM(CNAMS(JP))//'.nc'

 ZTEMP=0._R8
 N_OFFS=N_Z+N2
 DO JL=NMINS(JP)+N_OFFS,NMAXS(JP)+N_OFFS
      ZTEMP = ZTEMP + &
      & ZTEMP3(:,:,JL)*DZ(JL-N_OFFS)/ZSUM
 ENDDO
 CALL WRITE_VARNCDF(TRIM(CPR)//'sal_'//TRIM(CNAMS(JP))//'.nc',N_XM,N_YM,ZTEMP,'ssd','PSU',&
 & 'Salinity standard deviation')

 WRITE(72,*) 'sal_'//TRIM(CNAMS(JP))//'.nc'

 IF( N3 .EQ. 4 ) THEN
   ZTEMP=0._R8
   N_OFFS=2*N_Z+N2
   DO JL=NMINS(JP)+N_OFFS,NMAXS(JP)+N_OFFS
      ZTEMP = ZTEMP + &
      & ZTEMP3(:,:,JL)*DZ(JL-N_OFFS)/ZSUM
   ENDDO
   CALL WRITE_VARNCDF(TRIM(CPR)//'uzc_'//TRIM(CNAMS(JP))//'.nc',N_XM,N_YM,ZTEMP,'usd','m s-1',&
   & 'U-Current standard deviation')

   WRITE(72,*) 'uzc_'//TRIM(CNAMS(JP))//'.nc'

   ZTEMP=0._R8
   N_OFFS=3*N_Z+N2
   DO JL=NMINS(JP)+N_OFFS,NMAXS(JP)+N_OFFS
      ZTEMP = ZTEMP + &
      & ZTEMP3(:,:,JL)*DZ(JL-N_OFFS)/ZSUM
   ENDDO
   CALL WRITE_VARNCDF(TRIM(CPR)//'vmc_'//TRIM(CNAMS(JP))//'.nc',N_XM,N_YM,ZTEMP,'vsd','m s-1',&
   & 'U-Current standard deviation')

   WRITE(72,*) 'vmc_'//TRIM(CNAMS(JP))//'.nc'
 ENDIF

ENDDO

ENDIF

IF(LL_SEC_STDEV) THEN

 ALLOCATE( ZTE(28,N_Z,N3) )
 ALLOCATE( COU(28,N_Z) )

 COU=0
 ZTE=0._R8

 DO JL=1,N_Z
   DO JJ=1,N_YM
      DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JL+N2).GT. 0.5_R8 .AND. ABS(LAT(JI,JJ)).LT.70._R8) THEN
          NIND=INT((LAT(JI,JJ)+70._R8)/5._R8)+1
          COU(NIND,JL)=COU(NIND,JL)+1
          ZTE(NIND,JL,1)=ZTE(NIND,JL,1)+ZTEMP3(JI,JJ,JL+N2)
          ZTE(NIND,JL,2)=ZTE(NIND,JL,2)+ZTEMP3(JI,JJ,JL+N2+N_Z)
          IF( N3 .EQ. 4 ) THEN
            ZTE(NIND,JL,3)=ZTE(NIND,JL,3)+ZTEMP3(JI,JJ,JL+N2+2*N_Z)
            ZTE(NIND,JL,4)=ZTE(NIND,JL,4)+ZTEMP3(JI,JJ,JL+N2+3*N_Z)
          ENDIF
       ENDIF
      ENDDO
   ENDDO
 ENDDO

 DO JL=1,N_Z
   DO JI=1,28
      IF(COU(JI,JL).GT.0) THEN
         ZTE(JI,JL,:)=ZTE(JI,JL,:)/REAL(COU(JI,JL))
      ENDIF
   ENDDO
 ENDDO

 CALL WRITE_VARNCDF(TRIM(CPR)//'tem_sect.nc',28,N_Z,ZTE(:,:,1),'sd','',&
 & 'Temperature standard deviation')
 CALL WRITE_VARNCDF(TRIM(CPR)//'sal_sect.nc',28,N_Z,ZTE(:,:,2),'sd','',&
 & 'Salinity standard deviation')

 IF (N3 .EQ. 4 ) THEN
   CALL WRITE_VARNCDF(TRIM(CPR)//'uzc_sect.nc',28,N_Z,ZTE(:,:,3),'sd','',&
   & 'U-current standard deviation')
   CALL WRITE_VARNCDF(TRIM(CPR)//'vmc_sect.nc',28,N_Z,ZTE(:,:,4),'sd','',&
   & 'V-current standard deviation')
   WRITE(72,*) 'uzc_sect.nc'
   WRITE(72,*) 'vmc_sect.nc'
 ENDIF

 DEALLOCATE(ZTE,COU)

ENDIF


IF(LL_SEC_CORRS) THEN

 ALLOCATE(ZVCORR(N_KMT,N_KMT),CCORR(N_KMT))
 ZVCORR=0._R8

DO JLI=1,N_KMT
CCORR=0
DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI).GT. 0.5_R8 .AND. ABS(LAT(JI,JJ)).LT.60._R8) THEN
         DO JL=1,N_KMT
          IF(JL.NE.JLI) THEN
            IF(ZMSK(JI,JJ,JL).GT. 0.5_R8 .AND. ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JL).GT.0._R8) THEN
               CCORR(JL)=CCORR(JL)+1
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JL,JP)
               ENDDO
               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JL))
               ZVCORR(JLI,JL)=ZVCORR(JLI,JL)+ZCORR
            ENDIF
          ENDIF
         ENDDO
       ENDIF
   ENDDO
ENDDO

DO JL=1,N_KMT
   IF(CCORR(JL).GT.0) ZVCORR(JLI,JL)=ZVCORR(JLI,JL)/REAL(CCORR(JL))
   ZVCORR(JLI,JLI)=1._R8
ENDDO
ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'vcorr_TOT.nc',N_KMT,N_KMT,ZVCORR,'corr','0-1',&
& 'Vertical correlations')
WRITE(72,*) 'vcorr_TOT.nc'

!

ZVCORR=0._R8

DO JLI=1,N_KMT
CCORR=0
DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI).GT. 0.5_R8 .AND. ABS(LAT(JI,JJ)-40._R8).LT.20._R8) THEN
         DO JL=1,N_KMT
          IF(JL.NE.JLI) THEN
            IF(ZMSK(JI,JJ,JL).GT. 0.5_R8 .AND. ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JL).GT.0._R8) THEN
               CCORR(JL)=CCORR(JL)+1
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JL,JP)
               ENDDO
               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JL))
               ZVCORR(JLI,JL)=ZVCORR(JLI,JL)+ZCORR
            ENDIF
          ENDIF
         ENDDO
       ENDIF
   ENDDO
ENDDO

DO JL=1,N_KMT
   IF(CCORR(JL).GT.0) ZVCORR(JLI,JL)=ZVCORR(JLI,JL)/REAL(CCORR(JL))
   ZVCORR(JLI,JLI)=1._R8
ENDDO
ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'vcorr_NET.nc',N_KMT,N_KMT,ZVCORR,'corr','0-1',&
& 'Vertical correlations')
WRITE(72,*) 'vcorr_NET.nc'

!

ZVCORR=0._R8

DO JLI=1,N_KMT
CCORR=0
DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI).GT. 0.5_R8 .AND. ABS(LAT(JI,JJ)+40._R8).LT.20._R8) THEN
         DO JL=1,N_KMT
          IF(JL.NE.JLI) THEN
            IF(ZMSK(JI,JJ,JL).GT. 0.5_R8 .AND. ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JL).GT.0._R8) THEN
               CCORR(JL)=CCORR(JL)+1
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JL,JP)
               ENDDO
               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JL))
               ZVCORR(JLI,JL)=ZVCORR(JLI,JL)+ZCORR
            ENDIF
          ENDIF
         ENDDO
       ENDIF
   ENDDO
ENDDO

DO JL=1,N_KMT
   IF(CCORR(JL).GT.0) ZVCORR(JLI,JL)=ZVCORR(JLI,JL)/REAL(CCORR(JL))
   ZVCORR(JLI,JLI)=1._R8
ENDDO
ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'vcorr_SET.nc',N_KMT,N_KMT,ZVCORR,'corr','0-1',&
& 'Vertical correlations')
WRITE(72,*) 'vcorr_SET.nc'

!

ZVCORR=0._R8

DO JLI=1,N_KMT
CCORR=0
DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI).GT. 0.5_R8 .AND. ABS(LAT(JI,JJ)).LT.20._R8) THEN
         DO JL=1,N_KMT
          IF(JL.NE.JLI) THEN
            IF(ZMSK(JI,JJ,JL).GT. 0.5_R8 .AND. ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JL).GT.0._R8) THEN
               CCORR(JL)=CCORR(JL)+1
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JL,JP)
               ENDDO
               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JL))
               ZVCORR(JLI,JL)=ZVCORR(JLI,JL)+ZCORR
            ENDIF
          ENDIF
         ENDDO
       ENDIF
   ENDDO
ENDDO

DO JL=1,N_KMT
   IF(CCORR(JL).GT.0) ZVCORR(JLI,JL)=ZVCORR(JLI,JL)/REAL(CCORR(JL))
   ZVCORR(JLI,JLI)=1._R8
ENDDO
ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'vcorr_TRO.nc',N_KMT,N_KMT,ZVCORR,'corr','0-1',&
& 'Vertical correlations')

WRITE(72,*) 'vcorr_TRO.nc'

!

ZVCORR=0._R8

DO JLI=1,N_KMT
CCORR=0
DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI).GT. 0.5_R8 .AND. LAT(JI,JJ).GT.60._R8) THEN
         DO JL=1,N_KMT
          IF(JL.NE.JLI) THEN
            IF(ZMSK(JI,JJ,JL).GT. 0.5_R8 .AND. ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JL).GT.0._R8) THEN
               CCORR(JL)=CCORR(JL)+1
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JL,JP)
               ENDDO
               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JL))
               ZVCORR(JLI,JL)=ZVCORR(JLI,JL)+ZCORR
            ENDIF
          ENDIF
         ENDDO
       ENDIF
   ENDDO
ENDDO

DO JL=1,N_KMT
   IF(CCORR(JL).GT.0) ZVCORR(JLI,JL)=ZVCORR(JLI,JL)/REAL(CCORR(JL))
   ZVCORR(JLI,JLI)=1._R8
ENDDO
ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'vcorr_ARC.nc',N_KMT,N_KMT,ZVCORR,'corr','0-1',&
& 'Vertical correlations')

WRITE(72,*) 'vcorr_ARC.nc'

!

ZVCORR=0._R8

DO JLI=1,N_KMT
CCORR=0
DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI).GT. 0.5_R8 .AND. LAT(JI,JJ).LE.-55._R8) THEN
         DO JL=1,N_KMT
          IF(JL.NE.JLI) THEN
            IF(ZMSK(JI,JJ,JL).GT. 0.5_R8 .AND. ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JL).GT.0._R8) THEN
               CCORR(JL)=CCORR(JL)+1
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JL,JP)
               ENDDO
               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JL))
               ZVCORR(JLI,JL)=ZVCORR(JLI,JL)+ZCORR
            ENDIF
          ENDIF
         ENDDO
       ENDIF
   ENDDO
ENDDO

DO JL=1,N_KMT
   IF(CCORR(JL).GT.0) ZVCORR(JLI,JL)=ZVCORR(JLI,JL)/REAL(CCORR(JL))
   ZVCORR(JLI,JLI)=1._R8
ENDDO
ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'vcorr_ACC.nc',N_KMT,N_KMT,ZVCORR,'corr','0-1',&
& 'Vertical correlations')
WRITE(72,*) 'vcorr_ACC.nc'

DEALLOCATE(ZVCORR,CCORR)

ENDIF


IF(LL_MAP_CORRS) THEN

ALLOCATE(ZVCORR3(N_XM,N_YM,N_KMT))

DO JMP=1,NNMP

ZVCORR3=0._R8
JLI=NLI1(JMP)

DO JLI2=1,N_KMT

IF( MOD(JLI2,N_Z+N2) .EQ. 0 ) THEN

   ZVCORR3(:,:,JLI2)=0._R8

ELSEIF( JLI2.EQ.JLI ) THEN

   WHERE( ZMSK(:,:,JLI2) .GT. 0.5_R8 ) ZVCORR3(:,:,JLI2)=1._R8

ELSE

DO JJ=1,N_YM
   DO JI=1,N_XM
       IF(ZMSK(JI,JJ,JLI)+ZMSK(JI,JJ,JLI2).GT. 1.5_R8 .AND. &
       & ZTEMP3(JI,JJ,JLI).GT.0._R8 .AND. ZTEMP3(JI,JJ,JLI2).GT.0._R8) THEN
               
               ZCOVAR=0._R8
               DO JP=1,N_EOF
                  ZCOVAR=ZCOVAR + ZEIVEC(IREG(JI,JJ),JLI,JP) * ZEIVAL(IREG(JI,JJ),JP) * &
               & ZEIVEC(IREG(JI,JJ),JLI2,JP)
               ENDDO

               ZCORR=ZCOVAR/(ZTEMP3(JI,JJ,JLI)*ZTEMP3(JI,JJ,JLI2))

               ZVCORR3(JI,JJ,JLI2)=ZCORR

       ENDIF
   ENDDO
ENDDO


ENDIF

ENDDO

CALL WRITE_VARNCDF(TRIM(CPR)//'corrmap_'//TRIM(CMPNAM(JMP))//'.nc',N_XM,N_YM,N_KMT,ZVCORR3,'corr','0-1',&
& 'Vertical correlations')

WRITE(72,*) 'corrmap_'//TRIM(CMPNAM(JMP))//'.nc'

ENDDO

DEALLOCATE(ZVCORR3)

ENDIF

DEALLOCATE(ZMSK,ZTEMP,ZTEMP3)

CLOSE(72)

END SUBROUTINE POSTPROC_EOFS
