Subroutine handleerr(ist)
Use Netcdf
implicit none
integer, intent(in) :: ist
if(ist /= nf90_noerr) then
   print *, 'Error: ', trim(nf90_strerror(ist))
   call abor1( 'handleerr in read_errors')
endif
End subroutine handleerr
