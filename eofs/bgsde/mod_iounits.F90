MODULE IOUNITS

USE SET_KND, ONLY : I4

IMPLICIT NONE

!... STANDARD UNITS
INTEGER(KIND=I4), PARAMETER :: IOUNOUT=6
INTEGER(KIND=I4), PARAMETER :: IOUNERR=0
INTEGER(KIND=I4), PARAMETER :: IOUNIN=5
INTEGER(KIND=I4), PARAMETER :: IOUNEOF=18     ! EOFS
INTEGER(KIND=I4), PARAMETER :: IOUNLOG=6     ! LOGFILE

END MODULE IOUNITS
