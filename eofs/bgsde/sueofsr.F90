SUBROUTINE SUEOFSR

!-----------------------------------------------------------------------
!                                                                      !
! DEFINE FILTER CONSTANTS, EOFS, ETC.                                  !
! 2010 SUMMER REVISION, NETCDF SUPPORT                                 !
!                                                                      !
! VERSION 1: S.DOBRICIC 2006                                           !
!-----------------------------------------------------------------------

  USE SET_KND
  USE EOF_STR
  USE IOUNITS, ONLY : IOUNERR, IOUNLOG, IOUNOUT,IOUNEOF

  IMPLICIT NONE

  INTEGER(I4)                 :: NRG, NEC, K, NSPL, I, J
  INTEGER(I4)                 :: KSTP, NSLC,REGNUM, IERR
  INTEGER(I4) :: JPAR,JLEV,KK,KPARS,KRANK,KDIMS(3)

     WRITE(IOUNLOG,*)
     WRITE(IOUNLOG,*) '  /// EOFS SETUP'

     IF(ROS%EOGNX .LE. 0) ROS%EOGNX=GRD%IM
     IF(ROS%EOGNY .LE. 0) ROS%EOGNY=GRD%JM
     IF(ROS%EOGNZ .LE. 0) ROS%EOGNZ=GRD%KM
     IF(ROS%EOGNZ .NE. GRD%KM) THEN
          WRITE(IOUNERR,*) 'ROS%EOGNZ != GRD%KM, ',ROS%EOGNZ,GRD%KM
          WRITE(IOUNERR,*) '   THIS IS NOT SUPPORTED FOR THE TIME BEING'
          CALL ABOR1('SUEOFSR: VERTICAL DIMENSION OF EOFS AND FIRST GUESS MISMATCH')
     ENDIF

     IF(ROS%NREG .EQ. 0 ) ROS%NREG = GRD%IM*GRD%JM
     WRITE(IOUNLOG,*) ' EOF MODES    : ',ROS%NEOF
     WRITE(IOUNLOG,*) ' EOF REGIONS  : ',ROS%NREG
     WRITE(IOUNLOG,*) ' EOF POINTS   : ',ROS%KMT
     CALL FLUSH(IOUNLOG)

     ALLOCATE ( ROS%EVC( ROS%NREG, ROS%KMT, ROS%NEOF), ROS%EVA( ROS%NREG, ROS%NEOF) )

     WRITE(IOUNLOG,*) ' EOF ALLOCATED, CALLING READ_NCEOF'
     CALL FLUSH(IOUNLOG)

     CALL READ_NCEOF(TRIM(ROS%EOF_FILE),ROS%EOGNX,ROS%EOGNY,ROS%NREG,GRD%IM,GRD%JM,&
     & ROS%KMT,ROS%NEOF,ROS%EVA,ROS%EVC,GRD%REG,LSQUAREDEVA)

     WRITE(IOUNLOG,*) ' READ_NCEOF CALLED'
     CALL FLUSH(IOUNLOG)

     DO KK=1,ROS%NEOF
      DO J=1,GRD%JM
       DO I=1,GRD%IM
          ROS%EVC(GRD%REG(I,J),1:GRD%KM,KK)=ROS%EVC(GRD%REG(I,J),1:GRD%KM,KK)*GRD%MSK(I,J,:)
          ROS%EVC(GRD%REG(I,J),(GRD%KM+1):(2*GRD%KM),KK)=ROS%EVC(GRD%REG(I,J),(GRD%KM+1):(2*GRD%KM),KK)*GRD%MSK(I,J,:)
       ENDDO
      ENDDO
     ENDDO

END SUBROUTINE SUEOFSR
