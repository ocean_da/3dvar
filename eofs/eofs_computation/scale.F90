SUBROUTINE SCALE(NTS, NZ, NK, ZDATA, NOPT, N2, N3, STD )

! SCALE
! Scales the data with either
! parameter stdev (NOPT=1), level-by-level stdev (NOPT=2),
! or by dynamic height relationship (NOPT=3)
!
! A.Storto - 2011-12

USE SET_KND
USE MODEL
USE STDEV

IMPLICIT NONE

INTEGER, INTENT(IN) :: NTS, NZ, NK
REAL(R8), INTENT(INOUT) :: ZDATA(NTS,NZ)
INTEGER, INTENT(IN) :: NOPT, N2, N3
REAL(R8), INTENT(OUT) :: STD(NZ)

INTEGER :: LL, JS, JE, JL, KK, JPAR
REAL(R8), PARAMETER :: RHO_0 = 1020._R8, &
 & RN_ALPHA=0.0002_R8, RN_BETA=0.001_R8
REAL(R8) :: ZTMP

IF( N2 + N3*NK .NE. NZ ) &
           CALL ABOR1('INCONSISTENT DIMS IN SCALE ROUTINE')

IF( NOPT .EQ. 1 ) THEN
           DO JPAR=1,N3
              JS=(JPAR-1)*NK+1
              JE=JPAR*NK
              ZTMP=ST_DEV( ZDATA(:,JS:JE) )
              STD(JS:JE) = ZTMP
           ENDDO
           DO JPAR=1,N2
              LL=N3*NK+JPAR
              STD(LL) = ST_DEV( ZDATA(:,LL) )
           ENDDO
ELSEIF( NOPT .EQ. 2 ) THEN
           DO JS=1,NZ
              STD(JS) = ST_DEV( ZDATA(:,JS) )
           ENDDO
ELSEIF( NOPT .EQ. 3 ) THEN
           JPAR=1
           JS=LL+(JPAR-1)*NK+1
           JE=LL+JPAR*NK
           KK=0
           DO JL=JS,JE
              KK=KK+1
              STD(JL) = RN_ALPHA * DZ(KK) / RHO_0 
           ENDDO
           JPAR=2
           JS=LL+(JPAR-1)*NK+1
           JE=LL+JPAR*NK
           KK=0
           DO JL=JS,JE
              KK=KK+1
              STD(JL) = RN_BETA * DZ(KK) / RHO_0
           ENDDO
           IF(N3 .GT. 2 ) THEN
              KK=JE+1
              STD(JE:NZ) = 1._R8
           ENDIF
           DO JPAR=1,N2
              LL=N3*NK+JPAR
              STD(LL) = 1._R8
           ENDDO
ELSE
           CALL ABOR1('WRONG SCALE OPTION')
ENDIF

DO JL=1,NZ
   IF( STD(JL) .NE. 0._R8 ) &
   ZDATA(:,JL) = ZDATA(:,JL) / STD(JL)
ENDDO

END SUBROUTINE SCALE
