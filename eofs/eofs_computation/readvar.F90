SUBROUTINE READVAR(CFIN,CV,IM,JM,KM,ZZ)

USE SET_KND
USE NETCDF

IMPLICIT NONE

INTEGER, INTENT(IN) :: IM,JM,KM
CHARACTER(LEN=*), INTENT(IN) :: CFIN, CV
REAL(R8), INTENT(OUT)  :: ZZ(IM,JM,KM)

INTEGER :: NC, ID, STAT

STAT = NF90_OPEN(CFIN,NF90_NOWRITE,NC)
IF (STAT /= NF90_NOERR) CALL HANDLEERR(STAT)
STAT = NF90_INQ_VARID (NC, CV, ID)
IF (STAT /= NF90_NOERR) CALL HANDLEERR(STAT)
STAT = NF90_GET_VAR (NC, ID, ZZ)
IF (STAT /= NF90_NOERR) CALL HANDLEERR(STAT)

STAT = NF90_CLOSE(NC)
IF (STAT /= NF90_NOERR) CALL HANDLEERR(STAT)

END SUBROUTINE READVAR
