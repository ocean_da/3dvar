program extract_eof

implicit none

integer, parameter :: r8 = selected_real_kind(13,300)
integer, parameter :: i4 = selected_int_kind(9)
integer(i4), parameter :: ix=182,iy=149,iz=31
integer(i4), parameter :: kmt=2*iz,neof=10,nreg=ix*iy
real(r8) :: evar(neof, nreg),evcr(kmt,neof,nreg),tr(iz),sr(iz)
integer  :: iargc,ireg(ix,iy),k,jx,jy,jz,je,kkx,kky
character(len=300) :: cfile
logical :: llexist

kkx=122
kky=97

if(iargc().ne.1) Stop 'usage extract_eof <file>' 
Call Getarg(1,cfile)
inquire(file=cfile,exist=llexist)
write(*,*) '===> ',cfile
if(.not.llexist) Stop 'usage extract_eof <file>'

k=0
do jy=1,iy
 do jx=1,ix
  k=k+1
  ireg(jx,jy)=k
 enddo
enddo

call read_bineof(cfile,2,&
           & kmt,ix,iy,iz,nreg,neof,evar,evcr)

write(*,*) 'POINT (kkx,kky) = (-150,1.5) Equatorial Pacific'

write(*,*) 'First eigenvalue: absolute, percentual',&
     & evar(1,ireg(kkx,kky)),100.*evar(1,ireg(kkx,kky))/sum(evar(:,ireg(kkx,kky)))

write(*,*) 'First eigenvector, (T,S) and reconstructed signal(T,S)'

tr=0.
sr=0.
do jz=1,iz
 do je=1,neof
  tr(jz)=tr(jz)+evar(je,ireg(kkx,kky))*evcr(jz,je,ireg(kkx,kky))
  sr(jz)=sr(jz)+evar(je,ireg(kkx,kky))*evcr(jz+iz,je,ireg(kkx,kky))
 enddo
enddo
do jz=1,iz
  write(*,*) jz,evcr(jz,1,ireg(kkx,kky)),evcr(jz+iz,1,ireg(kkx,kky)),tr(jz),sr(jz)
enddo

contains

subroutine read_bineof(cfilebin,kpars,& 
           & kmt,im,jm,km,nreg,neof,evar,evcr)

  implicit none

  CHARACTER(LEN=*),INTENT(IN) ::  cfilebin
  INTEGER(i4),INTENT(IN)  :: kpars,kmt,im,jm,km,nreg,neof
  REAL(r8),INTENT(OUT)  :: evar(neof,nreg), evcr(kmt,neof,nreg)

  INTEGER(i4) :: k,nec,nrg,ntot,ipar,iouneof

  INTEGER(i4)                 :: iauxa(10),iauxs,ierr

  CHARACTER(LEN=70)            ::  clcomm
  CHARACTER(LEN=7)             ::  clgrid
  CHARACTER(LEN=4)             ::  clvers
  CHARACTER(LEN=3)             ::  cend
  INTEGER(i4),PARAMETER :: ICHKWD = 3141592

  iouneof=21

  write(*,*) ' Reading Binary EOF file'

  open(iouneof,FILE=cfilebin,FORM='unformatted',status='OLD',IOSTAT=IERR)
  if(ierr.ne.0) stop ' Cannot open binary file '

! Headers

  read(iouneof) clcomm
  read(iouneof) clgrid 
  read(iouneof) clvers
  read(iouneof)

  write(*,*) ' Grid: ',clgrid,'  Version: ',clvers

  read(iouneof) iauxa(1:7)

  if( iauxa(1) .ne. kpars .or. &
   &  iauxa(2) .ne. kmt   .or. &
   &  iauxa(3) .ne. im    .or. &
   &  iauxa(4) .ne. jm    .or. &
   &  iauxa(5) .ne. km    .or. &
   &  iauxa(6) .ne. nreg  .or. &
   &  iauxa(7) .ne. neof )  then

     write(*,*) ' DIMENSIONS MISMATCH while reading binary EOFs file'
     write(*,*) 
     write(*,*) ' Expected dimensions'
     write(*,*)  kpars,kmt,im,jm,km,nreg,neof
  
     stop 'Dimension mismatch in read_bineof'

  endif
  
  read(iouneof) iauxa(1:kpars+1)
  if(iauxa(kpars+1) .ne. ICHKWD ) stop 'Problems reading bineof'
  
  ntot = 0
  do ipar=1,kpars
     if(iauxa(ipar).eq. 3) ntot = ntot + km
     if(iauxa(ipar).eq. 2) ntot = ntot + 1
  enddo

  if( ntot .ne. kmt ) stop  'Problems Defining dimensions'

! Body

  !... Set No 1: Eigenvalues
  read(iouneof) ((evar(nec,nrg),nrg=1,nreg),nec=1,neof),iauxs
  if(iauxs .ne. ICHKWD ) stop 'Problems reading bineof'

  !... Set No 2: Eigenvectors
  do k=1,kmt
     read (iouneof) &
     & ((evcr(k,nec,nrg),nrg=1,nreg),nec=1,neof),iauxs
  if(iauxs .ne. ICHKWD ) stop  'Problems reading bineof'
  enddo
  read(iouneof) cend
  if (cend(1:3) .ne. 'END' ) stop  'Problems reading bineof'
  close(iouneof)

end subroutine read_bineof

end program extract_eof
