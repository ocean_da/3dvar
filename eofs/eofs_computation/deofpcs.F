
	subroutine deofpcs( data, nx, nt, nmodes, iminnxnt,
     &		imaxnxnt, evecs, doswitched, tentpcs )

c       ------------------------------------------------
c       Compute the tentative principal components; they
c       are 'tentative' because they might be the PCs
c       of the switched data.  Put them into 'tentpcs',
c       which is of size (nt,nmodes) if we are doing
c       regular EOFs and (nx,nmodes) if we are doing
c       switched EOFs.  These PCs come out in order
c       corresponding to the order of 'evecs', which is
c       in ASCENDING order.
c
c	Inputs:
c
c		data(nx,nt): The input data.  THESE MUST
c			BE ANOMALIES.
c
c		nmodes: # of modes to calculate.
c
c		iminnxnt: min(nx,nt)
c
c		evecs(iminnxnt,nmodes): the eigenvectors 
c			(which might be switched).
c
c		doswitched: if .TRUE., then we are doing
c			switched (space,time) calculation;
c			otherwise, regular (time,space)
c			calculation.
c
c	Outputs:
c
c		tentpcs(imaxnxnt,nmodes): the tentative
c			(possibly switched) principal
c			components.
c
c	David W. Pierce
c	Scripps Institution of Oceanography
c	Climate Research Division
c	dpierce@ucsd.edu
c	Jan 29, 1996
c       ------------------------------------------------

	implicit none

c	-----------------
c	Passed parameters
c	-----------------
	integer	nx, nt, nmodes, iminnxnt, imaxnxnt
	double precision data(nx,nt), evecs(iminnxnt,nmodes),
     &		tentpcs(imaxnxnt,nmodes)
	logical	doswitched

c	---------------
c	Local variables
c	---------------
	integer	i, j, k
	double precision	sum

	if( doswitched ) then
		do j=1, nmodes
		do i=1, nx
			sum = 0.0
			do k=1, nt
				sum = sum + data(i,k)*evecs(k,j)
			enddo
			tentpcs(i,j) = sum
		enddo
		enddo
	else
		do j=1, nt
		do i=1, nmodes
			sum = 0.0
			do k=1, nx
				sum = sum + data(k,j)*evecs(k,i)
			enddo
			tentpcs(j,i) = sum
		enddo
		enddo
	endif

	return
	end
