	program testeof

c	Tests the EOF routine. Input is Pacific sea surface temperatures
c	(SSTs).  Output should show the El Nino/La Nina signal.  You can
c	see the output plotted at:
c
c		http://meteora.ucsd.edu/~pierce/eof/eofs.html
c	

	implicit none

	integer nt, nx, nmodes, iminnxnt, imaxnxnt, nx_real, ny_real
	parameter(nt=29,nx_real=180,ny_real=60,nmodes=13)
	parameter(nx=nx_real*ny_real)
	parameter(iminnxnt=min(nx,nt), imaxnxnt=max(nx,nt))

	double precision 	data(nx,nt), eigenvalues(nmodes),
     &				eigenvectors(nx,nmodes),
     &				princomp(nt,nmodes), variance(nmodes),
     &				cumvariance(nmodes), value, time
	double precision	spacked(iminnxnt*(iminnxnt+1)/2),
     &				evals(iminnxnt), evecs(iminnxnt,nmodes),
     &				rlawork(8*iminnxnt), tentpcs(imaxnxnt,nmodes),
     &				sqrootweights(nx), lon(nx_real), 
     & 				lat(ny_real)
	integer			ilawork(5*iminnxnt),
     &				ifail(iminnxnt), i, j, k, idx

	integer icovcor

	print *, 'reading data'
	open(10,file='pacific_sst_test_data.txt',form='formatted',status='old')
	do k=1, nt
		idx = 0
		do j=1, ny_real
		do i=1, nx_real
			read(10,*) lon(i), lat(j), time, value
			idx = idx + 1
			if( value .lt. -100.0 ) then
				data(idx,k) = 0.0
			else
				data(idx,k) = value
			endif
		enddo
		enddo
	enddo

c	Set area weights to 1.0 for this test, although you could also compute 
c	the area given the lat and lon
	do i=1, nx
		sqrootweights(i) = 1.0
	enddo

	icovcor = 0	! use covariance matrix

	print *, 'calling deof'
	call deof( data, nx, nt, nmodes, icovcor,
     &		eigenvalues, eigenvectors, princomp, variance, cumvariance,
     &		iminnxnt, imaxnxnt, spacked, evals, evecs, rlawork, 
     &		ilawork, ifail, tentpcs, sqrootweights )

	print *, 'variance of top 3 modes:', 
     &		variance(1), variance(2), variance(3)
	print *, 'cumulative variance of top 3 modes: ', 
     &		cumvariance(1), cumvariance(2), cumvariance(3)

	open(10,file='eof_mode1.out',form='formatted')
	idx = 0
	do j=1, ny_real
	do i=1, nx_real
		idx = idx + 1
		write(10,'(2f7.1, f12.7)') lon(i), lat(j), eigenvectors(idx,1)
	enddo
	enddo
	close(10)

	open(10,file='pc_mode1.out',form='formatted')
	do j=1, nt
		write(10,'(f12.7)') princomp(j,1)
	enddo
	close(10)

	print *, 'output in files eof_mode1.out and pc_mode1.out'

	end
