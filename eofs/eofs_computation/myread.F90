PROGRAM RRBE

IMPLICIT NONE

   INTEGER, PARAMETER ::                &
      r4 = SELECTED_REAL_KIND( 6, 37),  &  ! real*4
      r8 = SELECTED_REAL_KIND(13,300)      ! real*8

   INTEGER, PARAMETER ::                &
      i4 = SELECTED_INT_KIND(9) ,       &  ! integer*4
      ib = SELECTED_INT_KIND(12) ,      &  !
      i8 = SELECTED_INT_KIND(14)           ! integer*8

  INTEGER(i4) :: jreg,kreg,jx,jy
  REAL(r8)  :: zevar(10,1472282), zevcr(100,10,1472282)

  INTEGER(i4) :: a1=2,a2=100,a3=1442,a4=1021,a5=50,a6=1472282,a7=10
CALL RRBE2('eof.bin',a1,a2,a3,a4,a5,a6,a7,zevar,zevcr)

jreg=0

do jy=1,1021
  do jx=1,1442

     jreg=jreg+1

     IF(jx.eq.258 .and. jy.eq.640) THEN
        kreg=jreg
     ENDIF

  enddo
enddo

   WRITE(701,'(10F13.9)') zevar(1:10,kreg)
do jx=1,100
   WRITE(701,'(10F13.9)') zevcr(jx,1:10,kreg)
enddo

CONTAINS

subroutine RRBE2(cfilebin,kpars,&
           & kmt,im,jm,km,nreg,neof,evar,evcr)

!... Write out in EOF in binary file -- For external use

  implicit none

  INTEGER(i4),INTENT(INOUT)  :: kmt,im,jm,km,nreg,neof
  INTEGER(i4),INTENT(INOUT)  :: kpars
  INTEGER(i4)  :: kparsl(kpars)
  REAL(r8),INTENT(OUT)  :: evar(neof,nreg), evcr(kmt,neof,nreg)

  INTEGER(i4) :: k,nec,nrg,ntot,ipar
  INTEGER(i4),PARAMETER :: iouneof = 210

  INTEGER(i4)                 :: iauxa(10),iauxs,ierr

  CHARACTER(LEN=70)            ::  clcomm
  CHARACTER(LEN=*)            ::  cfilebin
  CHARACTER(LEN=7)             ::  clgrid
  CHARACTER(LEN=4)             ::  clvers
  INTEGER(i4) :: ICHKWD = 3141592

! Headers

  open(iouneof,FILE=cfilebin,FORM='unformatted')

! Headers
  !... Write comments as separate records
  WRITE(*,*) 'READING COMM'
   read(iouneof) clcomm
   read(iouneof) clgrid
   read(iouneof) clvers
  !read(iouneof) 
  !read(iouneof) 
  !read(iouneof) 
  read(iouneof)
  !... Write dimensions
  read(iouneof) kpars,     kmt,    im,    jm,    km,    nreg,    neof
  ! List dimensionality of kpars
  read(iouneof) kparsl,ICHKWD
! Body
  !... Set No 1: Eigenvalues
  WRITE(*,*) 'READING EIVA'
  read(iouneof) ((evar(nec,nrg),nrg=1,nreg),nec=1,neof),ICHKWD
  !... Set No 2: Eigenvectors
  WRITE(*,*) 'READING EIVE'
  do k=1,kmt
     WRITE(*,*) 'READING ',k,kmt
     read(iouneof) &
     & ((evcr(k,nec,nrg),nrg=1,nreg),nec=1,neof),ICHKWD
  enddo
  close(iouneof)

end subroutine RRBE2

END PROGRAM RRBE
