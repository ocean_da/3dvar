MODULE set_knd

!-----------------------------------------------------------------------
!                                                                      !
! The precision of reals and integers                                  !
!                                                                      !
! Version 1: S.Dobricic 2006                                           !
!-----------------------------------------------------------------------


implicit none

public

   INTEGER, PARAMETER ::                &  
      r4 = SELECTED_REAL_KIND( 6, 37),  &  ! real*4
      r8 = SELECTED_REAL_KIND(13,300)      ! real*8

   INTEGER, PARAMETER ::                &  
      i4 = SELECTED_INT_KIND(9) ,       &  ! integer*4
      ib = SELECTED_INT_KIND(12) ,      &  !
      i8 = SELECTED_INT_KIND(14)           ! integer*8

END MODULE set_knd
