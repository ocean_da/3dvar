SUBROUTINE GET_VEL

!---------------------------------------------------------------------------
!                                                                          !
!    COPYRIGHT 2007 SRDJAN DOBRICIC, CMCC, BOLOGNA                         !
!                                                                          !
!    THIS FILE IS PART OF OCEANVAR.                                          !
!                                                                          !
!    OCEANVAR IS FREE SOFTWARE: YOU CAN REDISTRIBUTE IT AND/OR MODIFY.     !
!    IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  !
!    THE FREE SOFTWARE FOUNDATION, EITHER VERSION 3 OF THE LICENSE, OR     !
!    (AT YOUR OPTION) ANY LATER VERSION.                                   !
!                                                                          !
!    OCEANVAR IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,           !
!    BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        !
!    MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE         !
!    GNU GENERAL PUBLIC LICENSE FOR MORE DETAILS.                          !
!                                                                          !
!    YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     !
!    ALONG WITH OCEANVAR.  IF NOT, SEE <HTTP://WWW.GNU.ORG/LICENSES/>.       !
!                                                                          !
!---------------------------------------------------------------------------

!-----------------------------------------------------------------------
!                                                                      !
! CALCULATE HORIZONTAL VELOCITY FROM GEOSTROPHIC FORMULA               !
!                                                                      !
! VERSION 1: S.DOBRICIC 2007                                           !
! BUG CORRECTION 21.04.2009  THANKS TO ANDREA STORTO                   !
!-----------------------------------------------------------------------


  USE SET_KND
  USE BMD_STR
  USE GRD_STR

  IMPLICIT NONE

  REAL(R8), ALLOCATABLE, DIMENSION (:,:,:)  :: UD, VD
  REAL(R8), DIMENSION (GRD%IM,GRD%JM)  :: ZEBRTI,ZEBRTJ,ZEBRTJJ,ZEBRTB

  INTEGER(I4)    :: K, I, J, LR, I2, J2, XIND,YIND

  ALLOCATE ( UD(GRD%IM,GRD%JM,GRD%KM))
  ALLOCATE ( VD(GRD%IM,GRD%JM,GRD%KM))

  DO K=GRD%KM,2,-1
      GRD%B_X(:,:,K) = ( GRD%B_X(:,:,K) + GRD%B_X(:,:,K-1) ) * 0.5_R8
      GRD%B_Y(:,:,K) = ( GRD%B_Y(:,:,K) + GRD%B_Y(:,:,K-1) ) * 0.5_R8
  ENDDO
  GRD%B_X(:,:,1) = GRD%B_X(:,:,1) * 0.5_R8
  GRD%B_Y(:,:,1) = GRD%B_Y(:,:,1) * 0.5_R8

  GRD%UVL(:,:,:) = 0._R8
  GRD%VVL(:,:,:) = 0._R8

  UD(:,:,:) = 0._R8
  VD(:,:,:) = 0._R8

  ZEBRTI (:,:) = 0._R8
  ZEBRTJ (:,:) = 0._R8
  ZEBRTJJ(:,:) = 0._R8
  ZEBRTB (:,:) = 0._R8


    DO J=1,GRD%JM
       DO I=1,GRD%IM
          I2=XIND(I-1)
          J2=YIND(J-1)
          ZEBRTI(I,J) = (GRD%ETA(I,J)-GRD%ETA(I2,J)) / &
          & GRD%DX(I,J) * GRD%MSK(I,J,K)*GRD%MSK(I2,J,K)
          ZEBRTJ(I,J) = (GRD%ETA(I,J)-GRD%ETA(I,J2)) / &
          & GRD%DY(I,J) * GRD%MSK(I,J,K)*GRD%MSK(I,J2,K)
       ENDDO
    ENDDO

    DO J=BMD%NBJMIN,BMD%NBJMAX
       DO I=1,GRD%IM
          I2=XIND(I-1)
          J2=YIND(J-1)
          ZEBRTJJ(I,J) = (ZEBRTJ(I,J)-ZEBRTJ(I,J2)) / &
          & GRD%DY(I,J) * GRD%MSK(I,J,K)*GRD%MSK(I,J2,K)
       ENDDO
    ENDDO

    DO J=1,GRD%JM
       DO I=1,GRD%IM
          ZEBRTJ(I,J) = ZEBRTJ(I,J) - ZEBRTJ(I,BMD%NJEQ)*BMD%WPFY(I,J)
          ZEBRTJJ(I,J) = ZEBRTJJ(I,J) - ZEBRTJ(I,BMD%NJEQ)*BMD%WPFYY(I,J)
       ENDDO
    ENDDO

    DO J=BMD%NBJMIN,BMD%NBJMAX
       DO I=1,GRD%IM
          J2=YIND(J-1)
          ZBETAU = (GRD%F(I,J) - GRD%F(I,J2))/GRD%DY(I,J)
          ZUBRTB(I,J) = (ZEBRTJ(I,J)-ZEBRTJ(I,J2)) / &
          & GRD%DY(I,J) * GRD%MSK(I,J,K)*GRD%MSK(I,J2,K)
       ENDDO
    ENDDO


    DO K=1,GRD%KM

      DO J=1,GRD%JM
       DO I=1,GRD%IM
        I2=XIND(I-1)
        VD(I,J,K) = ( (GRD%ETA(I,J)-GRD%ETA(I2,J))*9.81 + GRD%B_X(I,J,K) ) &
                  / GRD%DX(I,J) * GRD%MSK(I,J,K)*GRD%MSK(I2,J,K) / GRD%F(I,J)
       ENDDO
      ENDDO

      DO J=2,GRD%JM
       DO I=1,GRD%IM
        UD(I,J,K) = - ( (GRD%ETA(I,J)-GRD%ETA(I,J-1))*9.81 + GRD%B_Y(I,J,K) ) &
                  / GRD%DY(I,J) * GRD%MSK(I,J,K)*GRD%MSK(I,J-1,K) / GRD%F(I,J)
       ENDDO
      ENDDO

    ENDDO

    DO K=1,GRD%KM
      DO J=1,GRD%JM
       DO I=1,GRD%IM
        I2=XIND(I-1)
        J2=YIND(J+1)
        GRD%UVL(I,J,K) = ( UD(I,J,K)+UD(I2,J,K)+UD(I,J2,K)+UD(I2,J2,K) )*0.25 * GRD%MSK(I,J,K)*GRD%MSK(I-1,J,K)
       ENDDO
      ENDDO
      DO J=1,GRD%JM      ! 2:GRD%JM
       DO I=1,GRD%IM
        I2=XIND(I+1)
        J2=YIND(J-1)
        GRD%VVL(I,J,K) = ( VD(I,J,K)+VD(I,J2,K)+VD(I2,J,K)+VD(I2,J2,K) )*0.25  * GRD%MSK(I,J,K)*GRD%MSK(I,J-1,K)
       ENDDO
      ENDDO
    ENDDO

  DEALLOCATE ( UD, VD )

END SUBROUTINE GET_VEL
