SUBROUTINE GET_VEL

!-----------------------------------------------------------------------
!                                                                      !
! CALCULATE VERTICAL INTEGRAL OF BOUYANCY GRADIENT                     !
!                                                                      !
! VERSION 1: S.DOBRICIC 2007                                           !
!-----------------------------------------------------------------------


  USE SET_KND
  USE GRD_STR
  USE BMD_STR
  USE LBCLNK

  IMPLICIT NONE

  REAL(R8), DIMENSION (GRD%IM,GRD%JM)  :: UD, VD, ZUF, ZUB
  REAL(R8), PARAMETER :: ZBETA = 2._R8*7.292115083046061E-5_R8/6371007.2_R8

  INTEGER(I4)    :: K, I, J
  INTEGER(I4)    :: KI, J2, JS1, JS2, ITER,KJ, I2, IS1, IS2
  REAL(R8) :: WWJ(9),WWI(3),TMP,TMP2,WWTI, WWTJ

     DO K=GRD%KM,2,-1
      GRD%B_X(:,:,K) = ( GRD%B_X(:,:,K) + GRD%B_X(:,:,K-1) ) * 0.5
      GRD%B_Y(:,:,K) = ( GRD%B_Y(:,:,K) + GRD%B_Y(:,:,K-1) ) * 0.5
     ENDDO
     GRD%B_X(:,:,1) = GRD%B_X(:,:,1) * 0.5
     GRD%B_Y(:,:,1) = GRD%B_Y(:,:,1) * 0.5

     GRD%UVL(:,:,:) = 0._R8
     GRD%VVL(:,:,:) = 0._R8

     ZUF=0._R8
     ZUB=0._R8

     DO K=1,GRD%KM

      UD(:,:) = 0.0
      VD(:,:) = 0.0

      VD(2:GRD%IM,:) =   ( (GRD%ETA(2:GRD%IM,:)-GRD%ETA(1:GRD%IM-1,:))*9.81 + GRD%B_X(2:GRD%IM,:,K) )               &
                           / GRD%DX(2:GRD%IM,:) * GRD%MSK(2:GRD%IM,:,K)*GRD%MSK(1:GRD%IM-1,:,K) / GRD%F(2:GRD%IM,:)
      VD(2:GRD%IM,:) = VD(2:GRD%IM,:)*(1._R8-BMD%WB(2:GRD%IM,:))
      IF( BMD%GLOBAL ) VD(1,:) = VD(GRD%IM-1,:)

      ZUF(:,2:GRD%JM) =  ( (GRD%ETA(:,2:GRD%JM)-GRD%ETA(:,1:GRD%JM-1))*9.81 + GRD%B_Y(:,2:GRD%JM,K) )               &
                         / GRD%DY(:,2:GRD%JM) * GRD%MSK(:,2:GRD%JM,K)*GRD%MSK(:,1:GRD%JM-1,K)

      DO J=BMD%NJEQ-20,BMD%NJEQ-1
           ZUB(:,J) = (  ZUF(:,BMD%NJEQ)-ZUF(:,J) ) / &
           & SUM(GRD%DY(GRD%IM/2,J:BMD%NJEQ)) * GRD%MSK(:,J,K)*GRD%MSK(:,BMD%NJEQ,K)
      ENDDO
      DO J=BMD%NJEQ+1,BMD%NJEQ+20
           ZUB(:,J) = (  ZUF(:,J)-ZUF(:,BMD%NJEQ) ) / &
           & SUM(GRD%DY(GRD%IM/2,BMD%NJEQ:J)) * GRD%MSK(:,J,K)*GRD%MSK(:,BMD%NJEQ,K)
      ENDDO

      UD(:,2:GRD%JM) = BMD%WB(:,2:GRD%JM)*(-ZUB(:,2:GRD%JM)/ZBETA) + &
      & (1._R8 - BMD%WB(:,2:GRD%JM))*(-ZUF(:,2:GRD%JM)/GRD%F(2:GRD%IM,:))

      ! FILTER

      CALL FILTER_UEQ(BMD%NFT_X,BMD%NW_X,BMD%NCF_X,BMD%NFT_Y,BMD%NW_Y,BMD%NCF_Y, &
                      GRD%IM,GRD%JM,GRD%MSK(:,:,K),BMD%NITER,BMD%NJEQ,UD)

      GRD%UVL(2:GRD%IM,1:GRD%JM-1,K) =                     &
      ( UD(2:GRD%IM,1:GRD%JM-1)+UD(1:GRD%IM-1,1:GRD%JM-1)+UD(2:GRD%IM,2:GRD%JM)+UD(1:GRD%IM-1,2:GRD%JM) )*0.25  &
                         * GRD%MSK(2:GRD%IM,:,K)*GRD%MSK(1:GRD%IM-1,:,K)
      GRD%VVL(1:GRD%IM-1,2:GRD%JM,K) =                     &
      ( VD(1:GRD%IM-1,2:GRD%JM)+VD(1:GRD%IM-1,1:GRD%JM-1)+VD(2:GRD%IM,2:GRD%JM)+VD(2:GRD%IM,1:GRD%JM-1) )*0.25  &
                         * GRD%MSK(:,2:GRD%JM,K)*GRD%MSK(:,1:GRD%JM-1,K)

     ENDDO

     CALL LBC_LNK(GRD%UVL)
     CALL LBC_LNK(GRD%VVL)

END SUBROUTINE GET_VEL
