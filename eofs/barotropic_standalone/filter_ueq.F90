SUBROUTINE FILTER_UEQ(NFT_X,NW_X,NCF_X,NFT_Y,NW_Y,NCF_Y,IM,JM,LSM,NITER,NJEQ,UD)

  USE SET_KND
  USE LBCLNK

  IMPLICIT NONE

  INTEGER(I4), INTENT(IN) :: NFT_X,NW_X,NCF_X,NFT_Y,NW_Y,NCF_Y
  INTEGER(I4), INTENT(IN) :: IM, JM, NITER, NJEQ
  REAL(R8), DIMENSION (IM,JM), INTENT(IN)  :: LSM
  REAL(R8), DIMENSION (IM,JM), INTENT(INOUT)  :: UD
  INTEGER(I4)    :: I, J
  INTEGER(I4)    :: KI, J2, JS1, JS2, ITER,KJ, I2, IS1, IS2
  REAL(R8) :: TMP,TMP2,WWTI, WWTJ,ZR
  REAL(R8), ALLOCATABLE :: WWJ(:),WWI(:)

  IF( NFT_X .EQ. 0 .AND. NFT_Y .EQ. 0 ) RETURN
  IF( NFT_X .NE. 0 ) THEN
      ZR = GRD%DX(IM/2,NJEQ)
      ALLOCATE(WWI(NW_X))
      CALL FTWEIGTHS( NFT_X,NW_X,NCF_X,ZR,WWI )
  ENDIF
  IF( NFT_Y .NE. 0 ) THEN
      ZR = GRD%DY(IM/2,NJEQ)
      ALLOCATE(WWJ(NW_Y))
      CALL FTWEIGTHS( NFT_Y,NW_Y,NCF_Y,ZR,WWJ )
  ENDIF

  DO ITER=1,NITER
      DO J=NJEQ-8,NJEQ+8
         DO I=2,IM-1
            JS1=J-(NW_Y-1)/2
            JS2=J+(NW_Y-1)/2
            IS1=MAX(1,I-(NW_X-1)/2)
            IS2=MIN(IM,I+(NW_X-1)/2)
            TMP=0._R8
            TMP2=0._R8
            KJ = 0
            DO J2=JS1,JS2
             KJ=KJ+1
             WWTJ=WWJ(KJ)
             KI = 0
             DO I2=IS1,IS2
               KI=KI+1
               WWTI=WWI(KI)
               TMP = TMP +WWTI*WWTJ*UD(I2,J2)*LSM(I2,J2)
               TMP2= TMP2+WWTI*WWTJ*LSM(I2,J2)
             ENDDO
            ENDDO
            IF( TMP2 .GT. 0._R8 ) UD(I,J)= TMP / TMP2
          ENDDO
       ENDDO
       CALL LBC_LNK(UD)
    ENDDO

END SUBROUTINE FILTER_UEQ
