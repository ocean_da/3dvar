SUBROUTINE WRITE_UVE

USE SET_KND
USE GRD_STR
USE NETCDF

IMPLICIT NONE

CALL WRITE_VARNCDF ('vozocrtx_bal.nc',GRD%IM,GRD%JM,GRD%KM,GRD%UVL,'vozocrtx','m/s','Zonal Current')
CALL WRITE_VARNCDF ('vomecrty_bal.nc',GRD%IM,GRD%JM,GRD%KM,GRD%VVL,'vomecrty','m/s','Meridional Current')
CALL WRITE_VARNCDFU('sossheig_bal.nc',GRD%IM,GRD%JM,     1,GRD%ETA,'sossheig',  'm','Sea Surface Height')

END SUBROUTINE WRITE_UVE
