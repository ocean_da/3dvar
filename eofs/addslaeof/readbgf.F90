subroutine Readbgf(ii,jj,kk,tb,sb)

use set_knd
Use netcdf

implicit none

integer(i4),intent(in) :: ii,jj,kk
real(r8),intent(out)   :: tb(ii,jj,kk)
real(r8),intent(out)   :: sb(ii,jj,kk)
integer(i4) :: stat,nc,id

stat = nf90_open('TB.nc',NF90_NOWRITE,nc)
if (stat /= nf90_noerr) call handleerr(stat)
stat = nf90_inq_varid (nc, 'votemper', id)
if (stat /= nf90_noerr) call handleerr(stat)
stat = nf90_get_var (nc, id, tb)
if (stat /= nf90_noerr) call handleerr(stat)
stat = nf90_close(nc)
if (stat /= nf90_noerr) call handleerr(stat)

stat = nf90_open('SB.nc',NF90_NOWRITE,nc)
if (stat /= nf90_noerr) call handleerr(stat)
stat = nf90_inq_varid (nc, 'vosaline', id)
if (stat /= nf90_noerr) call handleerr(stat)
stat = nf90_get_var (nc, id, tb)
if (stat /= nf90_noerr) call handleerr(stat)
stat = nf90_close(nc)
if (stat /= nf90_noerr) call handleerr(stat)

End subroutine Readbgf
